<?php
$bdd = new PDO('mysql:host=localhost;dbname=site_carte', 'root', '');

if(isset($_GET['id']) AND !empty($_GET['id']))
{
    $supprimer_id = htmlspecialchars($_GET['id']);

    $supprimer = $bdd->prepare('DELETE FROM article WHERE id = ?');
    $supprimer->execute(array($supprimer_id));

    header("Location: membre.php");
}
?>