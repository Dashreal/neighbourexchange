<?php
session_start();

$bdd = new PDO('mysql:host=localhost;dbname=site_carte', 'root', '');

$mode_edition = 0;


if (isset($_GET['edit']) AND !empty($_GET['edit']))
{
    $mode_edition = 1;
    $edit_id = htmlspecialchars($_GET['edit']);
    $edit_article = $bdd->prepare('SELECT * FROM article WHERE id = ?');
    $edit_article->execute(array($edit_id));

    if ($edit_article->rowCount() == 1) {
        $edit_article = $edit_article->fetch();
    } else {
        die('Erreur : l'article n\existe pas');
    }
}

if (isset($_POST['article_titre'], $_POST['article_contenu']))
{
    if (!empty($_POST['article_titre']) AND !empty($_POST['article_contenu']))
    {
        $article_titre = htmlspecialchars($_POST['article_titre']);
        $article_prix = htmlspecialchars($_POST['article_prix']);
        $article_contenu = htmlspecialchars($_POST['article_contenu']);

        if ($mode_edition == 0)
        {
            $ins = $bdd->prepare('INSERT INTO article (titre, prix, contenu, date_de_publication) VALUES(?, ?, ?, NOW())');
            $ins->execute(array($article_titre, $article_prix, $article_contenu));
            $lastid = $bdd->lastInsertId();

            $message = 'Votre annonce a bien été posté';
        }
        else
        {
            $update = $bdd->prepare('UPDATE article SET titre = ?, prix = ?, contenu = ?, date_de_edition = NOW() WHERE id = ?');
            $update->execute(array($article_titre, $article_prix, $article_contenu, $edit_id));
            header("Location: membre.php");
            $message = 'Votre article a bien été mise à jour';
        }
    }
    else
    {
        $message = 'Veuillez remplir tous les champs';
    }
}

?>