<?php  

require 'includes/data.php';

function getTitle($page){
    global $pagesTitles;
    return $pagesTitles[$page];
}

function getBody($page){
    include "pages/$page.php";
    //return "getBody : $page";
}

function getFooter($page){
    include 'partials/footer.php';
    //return "getFooter : $page";
}

function getPages(){
    global $pagesTitles;
    return array_keys($pagesTitles);   
}