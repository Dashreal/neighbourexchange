<?php 
  require_once 'includes/functions.php'; 



?>


<nav class="navbar navbar-expand-lg navbar-dark bg-dark" style="margin:24px 0;">
    <a class="navbar-brand" href="index.html"><img src="logo.ico" class="" alt="logo"></a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navb">
      <span class="navbar-toggler-icon"></span>
    </button>

    <ul class="navbar-nav">
      <?php foreach(getPages() as $navPage):
        $active = "";
        if($page == $navPage){
          $active = "active";
        }
        ?>
        <li class="nav-item <?= $active ?>">
          <a class="nav-link" href="index.php?page=<?= $navPage ?>"> <?= getTitle($navPage) ?> <span class="sr-only">(current)</span></a>
        </li>
      <?php endforeach ?>
    </ul>

    </div>
  </nav>