<!DOCTYPE html>
<html lang="fr">
<head>
  <title>Neighbour Exchange</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="style.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
  <style>
    .fakeimg {
      height: 200px;
      background: #aaa;
    }
  </style>
</head>
<body>

  <div class="jumbotron text-center" style="margin-bottom:0">
    <h1>Neighbour Exchange</h1>
    <p></p> 
  </div>

  <nav class="navbar navbar-expand-lg navbar-dark bg-dark" style="margin:24px 0;">
    <a class="navbar-brand" href="index.html"><img src="logo.ico" class="" alt="logo"></a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navb">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navb">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item">
          <a class="nav-link" href="compte.php">Profil</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="modifcompte.php">Modifier mon compte</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="services.php">Annonces</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="creaannonce.php">Crée une Annonce</a>
        </li>
      </ul>

    </div>
  </nav>

  
  <div class="jumbotron text-center" style="margin-bottom:0">
    <p>Copyright © Neighbour Exchange 2019 – 2020 – Tous droits réservés<br>
    Contact : NeighbourExchange@gmail.com</p>
  </div>

</body>