<!DOCTYPE html>
<html lang="fr">
<head>
  <title>Neighbour Exchange</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="style.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
  <style>
    .fakeimg {
      height: 200px;
      background: #aaa;
    }
  </style>
</head>
<body>

  <div class="jumbotron text-center" style="margin-bottom:0">
    <h1>Neighbour Exchange</h1>
    <p></p> 
  </div>




  <div class="container" style="margin-top:30px">
    <div class="row">
      <div class="col-sm-4">
        <h2>A propos :</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum id lectus gravida ante auctor lobortis. Nam faucibus, sapien nec dapibus sollicitudin, est lacus hendrerit justo, in tempor nisl lectus in mi. Proin pretium ante magna, et pharetra velit eleifend lobortis. Nullam finibus, felis ut mattis volutpat, tellus mi venenatis ipsum, sed tempor urna sapien sed erat. Quisque elementum tincidunt orci, eu efficitur elit suscipit eu. Nam ac tempor metus. Maecenas elementum pulvinar lectus non tristique.</p>
        <h3>Liens Externes :</h3>
        <p>Nos réseaux sociaux si dessous</p>
        <ul class="nav nav-pills flex-column">
          <li class="nav-item">
            <a class="nav-link" href="#">Facebook</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Twitter</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Instagram</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Linkedin</a>
          </li>
        </ul>
        <hr class="d-sm-none">
      </div>
      <div class="col-sm-8">
        <h2>Presentation :</h2>
        <div class="img"><img src="exchange.jpg" class="rounded" alt="exchange"></div>
        <p>Echange 24h/24</p>
        <p>Le but de Neighbour Exchange est de permettre à l’utilisateur de venir en aide mais aussi de demander de l’aide à ses voisins contre une rémunération.</p>
        <br>
        <h2>Actualités :</h2>
        <h5>Mis a jours le 7 Avril 2020</h5>
        <div class="img"><img src="actualite.jpg" class="rounded" alt="Actualités"></div>
        <p></p>
        <p>Nous vous annoncons officiellement l'ouverture de notre site. Vous pourrez voir que celui-ci a plusieur fonctionnalité.</p>
      </div>
    </div>
  </div>

  <div class="jumbotron text-center" style="margin-bottom:0">
    <p>Copyright © Neighbour Exchange 2019 – 2020 – Tous droits réservés<br>
    Contact : NeighbourExchange@gmail.com</p>
  </div>

</body>